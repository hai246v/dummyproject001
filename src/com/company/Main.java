package com.company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

enum TileKind {
    BOX, CHARACTER, EMPTY, GOAL, WALL
}

enum Direction {
    BOTTOM, LEFT, RIGHT, TOP
}

public class Main {
    private static Map map;
    private static ArrayList<Direction> actions;
    private static java.io.BufferedReader input;
    private static java.io.FileOutputStream output;
    private static boolean win = false;

    public static void main(String[] args) throws java.io.IOException {
        input = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(args[0])));
        output = new java.io.FileOutputStream("output.txt");
        loadMap();
        loadActions();
        actions.forEach(a -> {
            if (!win) processAction(a);
        });
        writeOutputLine(win ? "WIN" : "END");
    }

    public static void loadActions() throws IOException {
        actions = new ArrayList<Direction>(Arrays.asList(input.readLine()
                .split(" "))
                .stream()
                .map(s -> getDirectionFromString(s))
                .collect(Collectors.toList()));
    }

    public static void loadMap() throws IOException {
        ArrayList<String> mapData = new ArrayList<>();

        for (int i = 0; i < Map.HEIGHT; i++) {
            mapData.add(input.readLine());
        }
        map = new Map(String.join("", mapData));
    }

    public static void writeOutputLine(String s) {
        try {
            output.write((s + "\n").getBytes());
        } catch (Exception ignored) {

        }
    }

    public static Direction getDirectionFromString(String s) {
        return (s.equals("TOP") ? Direction.TOP : s.equals("BOTTOM") ? Direction.BOTTOM :
                s.equals("RIGHT") ? Direction.RIGHT : Direction.LEFT); // messed up. fixed.
    }

    public static void processAction(Direction action) {
        int newX = map.findCharacterPositionX() + (action == Direction.LEFT ? -1 : action == Direction.RIGHT ? 1 : 0);
        int newY = map.findCharacterPositionY() + (action == Direction.TOP ? -1 : action == Direction.BOTTOM ? 1 : 0);

        if (map.getTileAt(newX, newY).getKind() == TileKind.BOX)
            pushBox(action, newX, newY);
        if (map.getTileAt(newX, newY).getKind() == TileKind.GOAL)
            win = true;
        if (map.getTileAt(newX, newY).getKind() == TileKind.EMPTY || map.getTileAt(newX, newY).getKind() == TileKind.GOAL)
            changeCharacterPositionTo(newX, newY);

        writeCharacterPosition();
    }

    public static void writeCharacterPosition() {
        writeOutputLine(map.findCharacterPositionX() + ":" + map.findCharacterPositionY());
    }

    public static void pushBox(Direction action, int newX, int newY) {
        int newBoxX = newX + (action == Direction.LEFT ? -1 : action == Direction.RIGHT ? 1 : 0);
        int newBoxY = newY + (action == Direction.TOP ? -1 : action == Direction.BOTTOM ? 1 : 0);
        if (map.getTileAt(newBoxX, newBoxY).getKind() == TileKind.EMPTY)
            map.swapTile(newX, newY, newBoxX, newBoxY);
    }

    public static void changeCharacterPositionTo(int newX, int newY) {
        map.swapTile(map.findCharacterPositionX(), map.findCharacterPositionY(), newX, newY);
    }
}

class Tile {
    private TileKind myKind;

    public Tile(TileKind kind) {
        myKind = kind;
    }

    public TileKind getKind() {
        return myKind;
    }

    @Override
    public boolean equals(Object obj) {
        return myKind == ((Tile) obj).getKind();
    }
}

class Map {
    public static final int WIDTH = 20;
    public static final int HEIGHT = 10;

    public ArrayList<Tile> myTiles;

    public Map(String data) {
        myTiles = new ArrayList<>();

        for (int i = 0; i < WIDTH * HEIGHT; i++) {
            char c = data.charAt(i);
            myTiles.add(new Tile(c == ' ' ? TileKind.EMPTY : c == 'o' ? TileKind.BOX :
                    c == '#' ? TileKind.WALL : c == '@' ? TileKind.CHARACTER : TileKind.GOAL));
        }
    }

    public Tile getTileAt(int x, int y) {
        return myTiles.get(y * WIDTH + x);
    }

    public void setTileAt(int x, int y, Tile tile) {
        myTiles.set(y * WIDTH + x, tile);
    }

    public void swapTile(int x1, int y1, int x2, int y2) {
        Tile tempTile = getTileAt(x1, y1);
        setTileAt(x1, y1, getTileAt(x2, y2));
        setTileAt(x2, y2, tempTile);
    }

    public int findCharacterPositionX() {
        return myTiles.indexOf(new Tile(TileKind.CHARACTER)) % WIDTH;
    }

    public int findCharacterPositionY() {
        return myTiles.indexOf(new Tile(TileKind.CHARACTER)) / WIDTH;
    }

}
